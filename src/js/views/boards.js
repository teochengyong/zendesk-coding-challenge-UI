import React from 'react'
import Board from './board'
export default class Boards extends React.Component {
  render () {
    return (
      <div className='boards'>
        {
          this.props.boards.map(board => (
            <Board key={board.id} board={board} handleProjectMove={this.props.handleProjectMove} />
          ))
        }
      </div>
    )
  }
}
