import '../css/app.css'
import ReactDOM from 'react-dom'
import React from 'react'
import TodoApp from './views/todoApp'

ReactDOM.render(<TodoApp />, document.getElementById('todo'))
