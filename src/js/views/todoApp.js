import React from 'react'
import TodoTotal from './todoTotal'
import Boards from './boards'
export default class TodoApp extends React.Component {
  constructor (props) {
    super(props)
    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
    this.handleKeyPress = this.handleKeyPress.bind(this)
    this.handleProjectMove = this.handleProjectMove.bind(this)
    this.state = {
      boards: [
        {
          title: 'To Do',
          id: 'todo',
          projects: []
        },
        {
          title: 'In Progress',
          id: 'inProgress',
          projects: []
        },
        {
          title: 'done',
          id: 'done',
          projects: []
        }
      ],
      total: 0,
      text: ''
    }
  }

  render () {
    return (
      <div>
        <section id='header'>
          <div className='header_wrapper'>
            <div className='project_input'>
              <label>Add Project</label>
              <input type='text' onKeyPress={this.handleKeyPress} onChange={this.handleChange} value={this.state.text}></input>
            </div>
            <TodoTotal total={this.state.total} />
          </div>
        </section>
        <Boards boards={this.state.boards} handleProjectMove={this.handleProjectMove} />
      </div>
    )
  }

  handleChange (e) {
    this.setState({text: e.target.value})
  }

  handleSubmit (e) {
    e.preventDefault()
    var newItem = {
      text: this.state.text,
      id: Date.now()
    }
    this.setState((prevState) => ({
      boards: [
        {
          title: 'To Do',
          id: 'todo',
          projects: prevState['boards'][0]['projects'].concat(newItem)
        },
        {
          title: 'In Progress',
          id: 'inProgress',
          projects: prevState['boards'][1]['projects']
        },
        {
          title: 'done',
          id: 'done',
          projects: prevState['boards'][2]['projects']
        }
      ],
      total: prevState.total + 1,
      text: ''
    }))
  }

  handleKeyPress (e) {
    if (e.key === 'Enter') this.handleSubmit(e)
  }

  handleProjectMove (data) {
    let fromBoard = {}
    let toBoard = {}
    let fromProject = {}
    let toProject = {}

    let fromBoardResult = this.state.boards.filter(board => board.id === data.fromBoardId)
    if (fromBoardResult.length === 1) fromBoard = fromBoardResult[0]

    let toBoardResult = this.state.boards.filter((board) => {
      return board.id === data.toBoardId
    })
    if (toBoardResult.length === 1) toBoard = toBoardResult[0]

    let fromProjectResult = fromBoard.projects.filter((project) => {
      return project.id === parseInt(data.fromProjectId)
    })
    if (fromProjectResult.length === 1) fromProject = fromProjectResult[0]

    let toProjectResult = toBoard.projects.filter((project) => {
      return project.id === parseInt(data.toProjectId)
    })
    if (toProjectResult.length === 1) toProject = toProjectResult[0]

    this.setState((prevState) => {
      const newBoardsState = prevState.boards.map((board, index) => {
        let projects = prevState['boards'][index]['projects']
        if (fromBoard === toBoard && board.id === fromBoard.id) {
          const toProjectIndex = projects.findIndex((project) => project.id === parseInt(toProject.id))
          const fromProjectIndex = projects.findIndex((project) => project.id === parseInt(fromProject.id))
          const temp = projects[toProjectIndex]
          projects[toProjectIndex] = projects[fromProjectIndex]
          projects[fromProjectIndex] = temp
        } else if (fromBoard !== toBoard) {
          if (board.id === fromBoard.id) {
            const curProjectIndex = projects.findIndex((curProject) => curProject.id === fromProject.id)
            projects.splice(curProjectIndex, 1)
          } else if (board.id === toBoard.id) {
            projects.push(fromProject)
          }
        }
        return {
          title: board.title,
          id: board.id,
          projects: projects
        }
      })
      return Object.assign(prevState, {boards: newBoardsState})
    })
  }
}
