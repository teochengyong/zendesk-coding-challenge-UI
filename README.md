# Getting started

Install the dependencies and start the build process.
A http server will be available at the address mentioned in the console.

```
npm i 
npm start
```
# todo

We'd like to build a task management app. Features we're looking for:

* Entering text in the 'add project' input and hitting enter will add it as an item to the 'todo' list.
* Three columns for 'Todo', 'In Progress', and 'Done' projects.
* Projects should be draggable and sortable within the same column.
* Projects can also be dragged between adjacent columns.
* The total at the top of each column reflects the number of projects.
* The global total reflects the the global sum of projects.

Here's a static mock that shows how the complete project might look:

![screen shot 2015-10-28 at 11 43 12 am](https://cloud.githubusercontent.com/assets/279406/10799183/b3973b34-7d68-11e5-8a14-07c9b6f9310c.png)

Feel free to complete this project however you'd like (email us if you have any questions!), then email us a link or archive of the completed work. JSBin works fine, though a zipped project folder works great too.