const ExtractTextPlugin = require('extract-text-webpack-plugin')
module.exports = {
  entry: ['./src/js/app.js'],
  output: {
    filename: './assets/js/app.js'
  },
  devtool: 'source-map',
  module: {
    rules: [{
      test: /\.css$/,
      use: ExtractTextPlugin.extract({
        use: [
          {
            loader: 'css-loader',
            options: {
              importLoaders: 1
            }
          },
          'postcss-loader'
        ]
      })
    },
    {
      test: /\.js$/,
      loader: 'babel-loader',
      exclude: [/node_modules/],
      options: {
        'retainLines': true,
        'presets': [
          ['babel-preset-react']
        ]
      }
    }
    ]
  },
  plugins: [
    new ExtractTextPlugin('./assets/css/app.css')
  ]
}
