import React from 'react'
export default class Board extends React.Component {
  constructor (props) {
    super(props)
    this.dragStart = this.dragStart.bind(this)
    this.dragEnter = this.dragEnter.bind(this)
    this.dragOver = this.dragOver.bind(this)
    this.dragLeave = this.dragLeave.bind(this)
    this.drop = this.drop.bind(this)
    this.dragEnd = this.dragEnd.bind(this)
    this.state = {
      title: this.props.board.title,
      total: this.props.board.projects.length,
      projects: this.props.board.projects
    }
  }
  render () {
    return (
      <div className='board'>
        <div className='board_header'
          onDragStart={this.dragStart}
          onDragEnter={this.dragEnter}
          onDragOver={this.dragOver}
          onDragLeave={this.dragLeave}
          onDrop={this.drop}
          onDragEnd={this.dragEnd}
        >
          <div className='title'>{this.props.board.title}</div>
          <div className='total'>
            <span className='number'>{this.props.board.projects.length}</span>
            <span>PROJECTS</span>
          </div>
        </div>
        <ul className='projects'>
          {
            this.props.board.projects.map(project => (
              <li data-key={project.id}
                onDragStart={this.dragStart}
                onDragEnter={this.dragEnter}
                onDragOver={this.dragOver}
                onDragLeave={this.dragLeave}
                onDrop={this.drop}
                onDragEnd={this.dragEnd}
                className='project'
                key={project.id}
                draggable='true'>
                {project.text}
              </li>
            ))
          }
        </ul>
      </div>
    )
  }
  dragOver (ev) {
    ev.preventDefault()
    ev.dataTransfer.dropEffect = 'move'
    return false
  }
  dragStart (ev) {
    this.state.dragSrcEl = ev.target
    ev.dataTransfer.effectAllowed = 'move'
    const key = ev.target.dataset.key
    ev.dataTransfer.setData('project', JSON.stringify({
      fromBoardId: this.props.board.id,
      fromProjectId: key
    }))
  }

  dragEnter (ev) {
    ev.target.classList.add('over')
  }

  dragLeave (ev) {
    ev.target.classList.remove('over')
  }
  drop (ev) {
    ev.preventDefault()
    if (this.state.dragSrcEl !== ev.target) {
      const data = JSON.parse(ev.dataTransfer.getData('project'))
      if (ev.target.classList.contains('project')) {
        this.props.handleProjectMove(Object.assign(data, {
          toBoardId: this.props.board.id,
          toProjectId: ev.target.dataset.key
        }))
      } else if (ev.target.classList.contains('board_header')) {
        this.props.handleProjectMove(Object.assign(data, {toBoardId: this.props.board.id}))
      }
      ev.target.classList.remove('over')
    }
    return false
  }

  dragEnd (ev) {
    let projects = document.querySelectorAll('.project')
    let boardHeaders = document.querySelectorAll('.board_header')
    ev.target.removeAttribute('style')
    projects.forEach((col) => {
      col.classList.remove('over')
    })
    boardHeaders.forEach((col) => {
      col.classList.remove('over')
    })
  }
}
