module.exports = {
  plugins: [
    require('postcss-cssnext'),
    require('colorguard'),
    require('postcss-discard-empty')
  ]
}
