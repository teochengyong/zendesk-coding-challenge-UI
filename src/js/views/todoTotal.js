import React from 'react'
export default class TodoTotal extends React.Component {
  render () {
    return (
      <div className='grand_total'>
        <span>total</span>
        <div className='total'>
          <span className='number'>{this.props.total}</span>
          <span>PROJECTS</span>
        </div>
      </div>
    )
  }
}
